var app = {
	init: function() {
		app.browser_detect();
		app.carousel_main_front();
		app.lang_popup();
		app.menu_mobile();
		app.personal_popup();
		app.search_form_small();
		app.padding_for_under_header();
		app.toggle_box();
		app.nst_slider();
		app.input_number();
		app.select();
		app.multiple_select();
		app.stick_elems();
		app.catalog_favour();
		app.filter_catalog_front();
		app.filter_mob();
		app.queries_tables();
		app.input_pass_change_type();
		app.tabs();
		app.scroll();
		app.product_slider();
		app.product_slider_zoom();
	},
	browser_detect: function() {
		navigator.sayswho= (function(){
		  var ua= navigator.userAgent, tem, 
		  M= ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
		  if(/trident/i.test(M[1])){
			  tem=  /\brv[ :]+(\d+)/g.exec(ua) || [];
			  return 'IE '+(tem[1] || '');
		  }
		  if(M[1]=== 'Chrome'){
			  tem= ua.match(/\b(OPR|Edge)\/(\d+)/);
			  if(tem!= null) return tem.slice(1).join(' ').replace('OPR', 'Opera');
		  }
		  M= M[2]? [M[1], M[2]]: [navigator.appName, navigator.appVersion, '-?'];
		  if((tem= ua.match(/version\/(\d+)/i))!= null) M.splice(1, 1, tem[1]);
		  return M.join(' ');
		})();
		var result = navigator.sayswho;
		// console.log(result);
		return result;
	},
	carousel_main_front: function() {
		$('.carousel-main-front').owlCarousel({
			items: 1,
			loop: true,
			autoplay: true,
			autoplayTimeout: 5000,
			autoplayHoverPause: true,
			dots: true,
			animateIn: 'fadeInDown',
			animateOut: 'fadeOutDown'
		})
	},
	lang_popup: function() {
		var btn = $('.lang__link'),
		btn_parent = btn.parent(),
		link = $('.lang__popup a');
		btn.click(function() {
			$(this).parent().toggleClass('active');
		});
		$(document).on('click', function(e) {
			if(!$(e.target).closest(btn_parent).length) {
				btn_parent.removeClass('active');
			}
		});
		link.click(function() {
			btn_parent.removeClass('active');
		});
	},
	menu_mobile: function() {
		$('.header__hamburger').click(function() {
			$('.menu_mobile').addClass('visible');
			// body_scroll_hide(750);
		})
		$('.menu__close').click(function() {
			$('.menu_mobile').removeClass('visible');
		});

		// changing hamburger color on scroll
		
		if ( $('.wrap__main > *').length == false || $('.wrap__main > *:first-child').hasClass('background-color-white') || $('.wrap__main > *:first-child').hasClass('background-color-grey')  ) {
			$('.header__hamburger').css({'color': '#2a569f'});
			$('.header:not(.header_after_log)').css({'background-color': '#2a569f'});
			$('.button-login').addClass('button_on_background');
		}
		function change_color() {
			var scroll_top = $(window).scrollTop();
			$('.wrap > *, .wrap__main > *').each(function() {
				var target = $(this);
				if (target.offset().top - $('.header').outerHeight() - 30 <= scroll_top && target.offset().top + target.outerHeight()  - $('.header').outerHeight() - 30 > scroll_top) {
					if ( target.hasClass('background-color-white') || target.hasClass('background-color-grey') ) {
						$('.header__hamburger').css({'color': '#2a569f'});
					} else {
						$('.header__hamburger').css({'color': '#fff'});
					}
				}
				if (target.offset().top - $('.header').outerHeight() <= scroll_top && target.offset().top + target.outerHeight()  - $('.header').outerHeight() > scroll_top) {
					if ( target.hasClass('background-color-white') || target.hasClass('background-color-grey') ) {
						$('.header:not(.header_after_log)').css({'background-color': 'rgba(42, 86, 159, 1'});
						$('.button-login').addClass('button_on_background');
					} else {
						$('.header:not(.header_after_log)').css({'background-color': 'rgba(82, 87, 97, 0.18)'});
						$('.button-login').removeClass('button_on_background');
					}
				}
			});
		}
		$( window ).scroll(function() {
			change_color();
		});
	},
	personal_popup: function() {
		var btn = $('.personal__caret'),
		btn_parent = btn.parent().parent(),
		popup = btn_parent.find('.personal__popup');
		btn.click(function() {
			$(this).parent().parent().toggleClass('active');
		})
		$(document).on('click', function(e) {
			if(!$(e.target).closest(btn_parent).length) {
				btn_parent.removeClass('active');
			}
		})
	},
	search_form_small: function() {
		var flag = false;
		$('.search-form_header').submit(function() {
			if ( flag == false) {
				return false;
			} else {
				return true;
			}
		});
		$('.search-form__button').on('click', function() {;
			if ( $(this).hasClass('active') ) {
				flag = true;
			}
			$(this).toggleClass('active');
			$(this).parent().toggleClass('active').find('.search-form__input').attr('style', '');
		});
		$(document).on('click', function(e) {
			if(!$(e.target).closest('.search-form_header').length) {
				$('.search-form__button').removeClass('active').siblings('.search-form__input').css({'transition':'none'});
				$('.search-form_header').removeClass('active');
			}
		})
	},
	padding_for_under_header: function() {
		var header_height, pdt;
		// pdt = parseFloat($('.under-header-ban:first-child').css('padding-top'));
		function calc_header() {
			if ( $('.under-header-carousel:first-child').length == false && $('.under-header-ban:first-child').length == false ) {
				header_height = $('.header').outerHeight();
				$('.wrap__main').css({'padding-top': header_height});
			} else if ( $('.header_after_log').length && $('.under-header-ban:first-child').length ) {
				header_height = $('.header').outerHeight();
				$('.under-header-ban:first-child').css({'margin-top': header_height}).addClass('under-header-ban_size_small');
			} else if ( $('.header:not(.header_after_log)').length && $('.under-header-ban:first-child').length ) {
				$('.under-header-ban:first-child').addClass('under-header-ban_size_medium');
			}
		}
		calc_header();
		$(window).on('resize', function() {
			calc_header();
		})

	},
	toggle_box: function() {
		$('.toggle-box__caret').click(function() {
			$(this).parent().parent().toggleClass('active');
			$(this).parent().siblings().slideToggle();
		})
	},
	nst_slider: function() {
		$('.nstSlider').nstSlider({
			"left_grip_selector": ".leftGrip",
			"right_grip_selector": ".rightGrip",
			"value_bar_selector": ".bar",
			"value_changed_callback": function(cause, leftValue, rightValue) {
				$(this).find('.leftLabel').text(leftValue);
				$(this).find('.rightLabel').text(rightValue);
			}
		});
	},
	input_number: function() {
		var minus = $('.input-wrap__arrow_minus'),
		plus = $('.input-wrap__arrow_plus');
		plus.click(function(){
			var qty = $(this).parent().find('.input-number'),
			count = parseInt(qty.val());
			qty.val(++count);
		});
		minus.click(function(){
			var qty = $(this).parent().find('.input-number'),
			count = parseInt(qty.val());
			if (count > 0) {
				qty.val(--count);
			}
		});
	},
	select: function() {
		$('.select').niceSelect();

		$('.select ul').removeClass('list').wrap('<div class="select__scroll list"></div>')
	},
	multiple_select: function() {
		$('.select-multiple').multiselect({
			enableFiltering: true,
			enableCaseInsensitiveFiltering: true,
			buttonText: function(options, select) {
				select.attr('data-name');
				if (options.length === 0) {
					return $('option:first', select).parent().attr('data-name');
				}
				else if (options.length > 3) {
					return 'Выбрано больше 3х значений!';
				}
				else {
					var labels = [];
					options.each(function() {
						if ($(this).attr('label') !== undefined) {
							labels.push($(this).attr('label'));
						}
						else {
							labels.push($(this).html());
						}
					});
					return labels.join(', ') + '';
				}
			},
			templates: {
				ul: '<div class="select-multiple__container dropdown-menu animated zoomIn"></div>',
				filter: '<div class="select-multiple__item multiselect-item multiselect-filter"><div class="input-group"><span class="input-group-addon"><span class="select-multiple__icon  icon  icon_search"></span></span><input class="select-multiple__search-input  form-control multiselect-search input" type="text"></div></div>',
				li: '<li class="select-multiple__item"><a tabindex="0"><div class="multiselect-checkbox"><label for="" class="multiselect-checkbox__label"></label></div></a></li>',
			},
		});
		$('.select-multiple__container > *:not(.multiselect-filter)').wrapAll('<div class="select-multiple__scroll"><ul class="list"></ul></div>');
		$('.select-multiple__container .multiselect-checkbox').each(function(index) {
			var id = 'multiselect-' + index,
			$input = $(this).find('input');
			$(this).find('label').attr('for', id);
			$input.attr('id', id);
			$input.detach();
			$input.prependTo($(this));
			$(this).click(function(e) {
				e.stopPropagation();
			});
		});
	},
	stick_elems: function() {
		var header_height;
		function calc_header() {
			header_height = $('.header').outerHeight();
		}
		calc_header();
		$(window).on('resize', function() {
			calc_header();
		})
		function stick_filter() {
			if( $(window).width() > 768 ) {
				$(".catalog-filter__in").stick_in_parent({
					offset_top: header_height + 30
				})
			} else {
				$(".catalog-filter__in").trigger("sticky_kit:detach");
			}
		}
		stick_filter();
		$(window).on('resize', function() {
			stick_filter();
		})
	},
	catalog_favour: function() {
		$('.preview-good-catalog__favour').click(function(e) {
			e.preventDefault();
			$(this).toggleClass('active');
		})
		$('.preview-good-catalog__favour').mouseover(function() {
			$(this).removeClass('no-transitions');
		})
	},
	filter_catalog_front: function() {
		$('.filter-result-search__btn').click(function() {
			$('.filter-result-search__btn').removeClass('active');
			$('.preview-good-catalog__favour').addClass('no-transitions')
			$(this).addClass('active');
			if( $(this).hasClass('filter-result-search__btn_row') ) {
				$('.catalog-filter-cols')
				.addClass('catalog-filter-rows')
				.find('.catalog-filter-cols__item')
				.addClass('col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12')
				.find('.preview-good-catalog')
				.removeClass('flex-column').addClass('flex-row preview-good-catalog_in-line')
				.find('.preview-good-catalog__favour')
				.addClass('no-transitions');
			} else {
				$('.catalog-filter-cols')
				.removeClass('catalog-filter-rows')
				.find('.catalog-filter-cols__item')
				.removeClass('col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12')
				.find('.preview-good-catalog')
				.addClass('flex-column').removeClass('flex-row preview-good-catalog_in-line');
			}
		})
	},
	filter_mob: function() {
		$('.catalog__hamburger').click(function() {
			$('.catalog-filter_mob').addClass('visible');
		})
		$('.catalog-filter__close').click(function() {
			$('.catalog-filter_mob').removeClass('visible');
		})
	},
	scroll: function() {
		$(".select-multiple__scroll").mCustomScrollbar();
		$(".select__scroll").mCustomScrollbar();
	},
	queries_tables: function() {
		$('.queries-table__button_more').click(function() {
			$(this).parents('tr').next().find('.queries-table__detail').slideToggle();
		})
	},
	input_pass_change_type: function() {
		var flag = true;
		$('.popup-form__btn_change_type').click(function() {
			if(flag) {
				$(this).siblings('input[type="password"]').attr('type', 'text');
				flag = false;
			} else {
				flag = true;
				$(this).siblings('input[type="text"]').attr('type', 'password');
			}
		});
		$('.popup-form').submit(function() {
			if ( $('.popup-form__btn_change_type').siblings('.popup-form__input').attr('type', 'text') ) {
				$('.popup-form__btn_change_type').siblings('.popup-form__input').attr('type', 'password');
			}
		})
	},
	masonry_obj: function() {
		var msnry = $('.news-grid').masonry({
			itemSelector: '.news-grid__item',
			percentPosition: true,
			resize: true
		})
		msnry.on( 'layoutComplete', function( msnryInstance, laidOutItems ) {
			if ( laidOutItems.length == 17 ) {
				$('.news-grid').addClass('news-grid_items-align-end');
			} else {
				$('.news-grid').removeClass('news-grid_items-align-end');
			}
			
		});

		msnry.masonry();
	},
	tabs: function() {
		$('.tabs a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
			app.masonry_obj();
		}).on('hide.bs.tab', function(e) {
			$('.news-grid').masonry('destroy');
		});
		$('.tabs a[data-toggle="tab"]:first').trigger("shown.bs.tab");
		
	},
	product_slider: function() {
		// var $zoom = $('.zoomImage').magnify({
		// 	speed: 300,
		// 	limitBounds: true
		// });
		// var $sync1 = $('.product-slider').owlCarousel({
		// 	items: 1,
		// 	dots: false,
		// 	nav: false,
		// 	margin: 10,
		// 	startPosition: 3,
			// onTranslated: function() {
			// 	$zoom.destroy().magnify();
			// }
		// });
		var $sync2 = $('.product-slider-thumb').owlCarousel({
			items: 4,
			margin: 10,
			loop: true,
			navContainer: '.product-slider-nav',
			navText: ['<span class="icon-svg icon-svg_chevron"></span>','<span class="icon-svg icon-svg_chevron"></span>'],
			responsive: {
				0: {
					items: 2
				},
				575: {
					items: 4
				}
			}
		});


		

		// flag = false,
		// duration = 300;

		// $sync1.on('changed.owl.carousel', function (e) {
		// 	if (!flag) {
		// 		flag = true;
		// 		$sync2.trigger('to.owl.carousel', [e.item.index, duration, true]);
		// 		flag = false;
		// 	}
		// });

		// $sync2.on('click', '.owl-item', function () {
		// 	$sync1.trigger('to.owl.carousel', [$(this).index(), duration, true]);
		// }).on('changed.owl.carousel', function (e) {
		// 	if (!flag) {
		// 		flag = true;
		// 		$sync1.trigger('to.owl.carousel', [e.item.index, duration, true]);
		// 		flag = false;
		// 	}
		// });

	},
	product_slider_zoom: function() {
		// $('.zoomImage').wrap('<span style="display:inline-block"></span>').css('display', 'block').parent().zoom();
		$(".zoomImage, .zoomImage-gallery").xzoom({
			tint: '#333',
			Xoffset: 15,
			fadeOut: true,
			tintOpacity: 0.1
		});
	}
};
$(document).ready(function() {
	app.init();
	
});


$(window).on("load",function(){
	
});


